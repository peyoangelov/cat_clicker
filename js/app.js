//creatting the empty array fot the cats.
let catArray = [];

//creating class Cat and its constructor with atributtes catName CatPic and counter
//insert a _ to the attribute's name to escape ifinity loop when I using the setter
class Cat {
  constructor(catName, catPic){
     this._catName = catName;
     this._catPic = catPic;
     this._counterClick = 0;
  }

 }

//after the page is loaded here is created all elements, objects and the array is filled
function onReady(){
	let cat1 = new Cat("TOM", "images/cat1.jpg");
	let cat2 = new Cat("cat2","images/cat2.jpg");
	let cat3 = new Cat("cat3", "images/cat3.jpg");
	let cat4 = new Cat("cat4", "images/cat4.jpg");
	let cat5 = new Cat("cat5", "images/cat5.jpg");
	let cat6 = new Cat("cat6", "images/cat6.jpg");
	catArray.push(cat1);
	catArray.push(cat2);
	catArray.push(cat3);
	catArray.push(cat4);
	catArray.push(cat5);
	catArray.push(cat6);

	//loop into all cats in the array, 
	$.each(catArray, function(index,element){
		//checking is that first cat in the array and adding a class active to it 
		let firstCat = index == 0 ? "active" : "";
		//creating a html container
		let htmlCat = "<div id='catContainer_"+index+"' class='htmlCatHolder "+firstCat+"'>"  
						+"<h2 class = 'htmlCatName'>"+element._catName +" </h2>"
						+"<img id ='catImg_"+index+"' class = 'htmlCatImg'src='"+ element._catPic +"'/>" 
						+"<label id = 'htmlCatCounter_"+index+"'>Number of clicks: "+ element._counterClick +"</label>"
		 			+"</div>"
		 //appending the class holder form html			
		 $( ".holder" ).append(htmlCat);
		 //creating button for every cat
		 let htmlCatButton = "<button class='navigationButton "+firstCat+"' id='buttonCatElement_"+index+"'>" +(element._catName)+"</button>"
		//	appending the class buttonsHolder form html							
		 $( ".buttonsHolder" ).append(htmlCatButton);

	})

	//event listener for every button. 
	//with id^= I search the Id with only one part of the Id name which is the same for all Ids
	$("button[id^='buttonCatElement']").click(function(e){
			let catIndex =$(e.target).attr("id").substring(17);//get the button which is clicked from the click event
			$(".htmlCatHolder").removeClass("active");
			$('#catContainer_'+catIndex).addClass("active");	
			$(".navigationButton").removeClass("active"); //removing class active for every button			
			$(e.target).addClass("active");	//adding class active only for the clicked button
			fillCatInfo(); // if the admin pannel is opened, we update the cat info in the pannel
    });

	//event listener for click on the emage and increments the cat counterClick
	$("img[id^='catImg']").click(function (e){
	 		let idOfTheCat = e.target.id;
			let catIndex = idOfTheCat.substring(7);
			let cat = returnCat(catIndex);
			cat._counterClick++;
			$('#htmlCatCounter_'+catIndex).html('Number of clicks: '+cat._counterClick);	
			if ($(".holderAdmin").hasClass("active")) {
				$("#inputEditClicks").val(cat._counterClick);
			}	
	});
	//event listener for admin button
	$(".admin").click(function(){
		fillCatInfo();
		$(".holderAdmin").addClass("active");
	});

	$("#adminCancelButton").click(function(){
		$(".holderAdmin").removeClass("active");
	});

	$("#adminSaveButton").click(function(){
		let catToEditId = $(".navigationButton.active").attr("id").substring(17);
		let catToEdit = returnCat(catToEditId);
		let newCatName= $("#inputEditName").val();
		let newCatImg = $("#inputEditImg").val();
		let newCatCounter= $("#inputEditClicks").val();
		if (newCatName != catToEdit._catName ) {			
			catToEdit._catName = newCatName;
			$("#catContainer_"+catToEditId).find("h2").html(newCatName);
			$(".navigationButton.active").html(newCatName);			
		}
		if (newCatImg != catToEdit.catPic ) {			
			catToEdit._catPic =  newCatImg;
			$("#catImg_"+catToEditId).attr("src", newCatImg);
		}
		if (newCatCounter != catToEdit.counterClick ) {
			if(!isNaN(newCatCounter)){
				catToEdit._counterClick =  newCatCounter;
				$("#htmlCatCounter_"+catToEditId).html("Number of clicks: "+newCatCounter);
			}	
			else{
				alert("Please, insert a number!")
			}
		}
		$(".holderAdmin").removeClass("active");	
	});
	
};

//detects is the page loaded
$(document).ready(function() {
   onReady(); 
});
	//function which returns cat form the array on its name
function returnCat(catIndex){
	return catArray[catIndex];
}

function fillCatInfo(){
			let catToEditId = $(".navigationButton.active").attr("id").substring(17);//extract from the id of the clicked button the cat unique id
			let catToEdit = returnCat(catToEditId);
			$("#inputEditName").val(catToEdit._catName);
			$("#inputEditImg").val(catToEdit._catPic);
			$("#inputEditClicks").val(catToEdit._counterClick);
} 

